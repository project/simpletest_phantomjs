// see http://net.tutsplus.com/tutorials/javascript-ajax/testing-javascript-with-phantomjs/

var page = require('webpage').create();

var args = require('system').args;
if (args.length < 3) {
  console.log('Missing argument url (example: http://google.com/) provided arguments were ' . args);
}

if (args[2]) {
  page.settings.userAgent = args[2];
}

page.open(args[1], function (s) {
  setTimeout(console.log(page.content), 3000);
  setTimeout(phantom.exit(), 3000);
});

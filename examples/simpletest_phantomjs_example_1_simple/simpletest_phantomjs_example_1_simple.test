<?php

/**
 * @file
 * Automated functional tests for Simpletest PhantomJS Example.
 */

/**
 * Tests the functionality of Simpletest PhantomJS Example.
 */
class SimpletestPhantomJSExample1TestCase extends DrupalWebTestCase {

  /**
   * Info about this test.
   */
  public static function getInfo() {
    return array(
      'name' => 'Simpletest PhantomJS Example 1',
      'description' => 'Simpletest PhantomJS Example 1.',
      'group' => 'Simpletest PhantomJS',
    );
  }

  /**
   * Basic setup for all test cases.
   */
  public function setUp() {
    parent::setUp(array('simpletest_phantomjs_example_1_simple'));
  }

  /**
   * Main test case.
   */
  public function testSimpletestPhantomJSExample() {
    $this->drupalCreateNode(array('title' => 'example1'));
    $this->drupalGet('simpletest_phantomjs_example_1_simple');
    $this->assertText('original test', 'Without Javascript, original text appears.');
    $this->assertNoText('changed by javascript', 'Without Javascript, changed text does not appear.');
  }

}

if (!class_exists('SimpletestPhantomJSWebTestCase')) {
  drupal_set_message(t('SimpletestPhantomJSExampleTestCase test is not appearing because simpletest_phantomjs must be active'), 'warning');
  return;
}

/**
 * Tests the functionality of Simpletest PhantomJS Example.
 */
class SimpletestPhantomJSExample1JSTestCase extends SimpletestPhantomJSWebTestCase {

  /**
   * Info about this test.
   */
  public static function getInfo() {
    return array(
      'name' => 'Simpletest PhantomJS Example 1 javascript test',
      'description' => 'Simpletest PhantomJS Example 1 javascript test.',
      'group' => 'Simpletest PhantomJS',
    );
  }

  /**
   * Basic setup for all test cases.
   */
  public function setUp() {
    parent::setUp(array('simpletest_phantomjs_example_1_simple'));
  }

  /**
   * Main test case.
   */
  public function testSimpletestPhantomJSExample() {
    $this->drupalGet('simpletest_phantomjs_example_1_simple');
    $this->assertNoText('original test', 'Without Javascript, original text no longer appears.');
    $this->assertText('changed by javascript', 'Without Javascript, changed text appears.');
  }

}

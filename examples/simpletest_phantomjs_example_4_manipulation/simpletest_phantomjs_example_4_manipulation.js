(function ($) {

Drupal.behaviors.simpletest_phantomjs_example = {
  attach: function (context) {
    var mySpecialLink = $('#simpletest_phantom_js_latest');

    $('#simpletest_phantom_js_to_replace').html('changed by javascript');
    $('#simpletest_phantom_js_latest').load('http://localhost/drupal7sandbox/simpletest_phantomjs_example/ajax');
/*    var timer = setInterval( function() {
      $('#simpletest_phantom_js_latest').load('simpletest_phantomjs_example/ajax');
    }, 1000); */

/*      new Drupal.ajax('#simpletest_phantom_js_latest', mySpecialLink, {
        url: 'simpletest_phantomjs_example/ajax',
        settings: {},
        event: 'click tap',
      });
*/
/*
      var ajax = jQuery.getJSON('simpletest_phantomjs_example/ajax');
    }).done( function(response) {
      alert(response.response);
    });
*/
    // Because of Javascript prototypes, we don't need to change our javascript
    // code to take advantage of mocking.
/*    setTimeout( function() {
      var ajax = jQuery.getJSON('simpletest_phantomjs_example/ajax');
      $('#simpletest_phantom_js_latest').html(ajax.reponse);
      alert(ajax.reponse);
    }, 1000); */
  }
};

})(jQuery);

Simpletest PhantomJS
====================

Adds some functionality to Simpletest tests; see Simpletest PhantomJS Example
(simpletest_phantomjs_example) for sample usage.